import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RecipesComponent } from './recipes/recipes.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FeedbackComponent } from './feedback/feedback.component';
import {Router, RouterModule, Routes} from "@angular/router";
import { HomeComponent } from './home/home.component';
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { FavoriteComponent } from './favorite/favorite.component';
import { CartComponent } from './cart/cart.component';
import { LoginComponent } from './login/login.component';
import {AuthInterceptor} from './auth-interceptor';

const appRoutes :Routes =[
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'recipes',
    component:RecipesComponent
  },
  {
    path:'feedback',
    component:FeedbackComponent
  },
  {
    path:'login',
    component:LoginComponent
  },

  {
    path:'favorites',
    component:FavoriteComponent
  },
  {
    path:'cart',
    component:CartComponent
  },
  {
    path:'',
    component:RecipesComponent,
    pathMatch:'full'
  },
  {
    path:'**',
    component:NotFoundComponent
  }

];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    RecipesComponent,
    NotFoundComponent,
    FeedbackComponent,
    HomeComponent,
    FavoriteComponent,
    CartComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes,{enableTracing:true})

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS,
     useClass: AuthInterceptor,
    multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  public getCookie(name: string) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");

    if (parts.length == 2) {
      return parts.pop().split(";").shift();
    }
  }
}
