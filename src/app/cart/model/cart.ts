export interface Cart {
  id: number;
  uri: string;
  userId: number;
  label: string;
  ingredients: string[];
}
