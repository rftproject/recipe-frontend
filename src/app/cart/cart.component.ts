import { Component, OnInit } from '@angular/core';
import {Cart} from './model/cart';
import {ApiService} from '../shared/api.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  carts: Cart[] = [];

  public getCookie(name: string) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");

    if (parts.length == 2) {
      return parts.pop().split(";").shift();
    }
  }

  constructor(private apiService: ApiService) {
    this.getCarts(Number(this.getCookie('userId')));
  }

  ngOnInit() {
  }

  public getCarts(id: number) {
    this.apiService.getCartByUser(id).subscribe(
      res => {
        this.carts = res;
      },
      error =>{
        alert("An error has occured")
      }
    );
  }


  deleteCart(cartId: number): void{
    this.apiService.postDeleteCartById(cartId).subscribe(
      res => {
        location.reload();
      },
      res=>{
        alert("An error has occured while deleting cart");
      }
    );
  }

}

  export  interface CartViewModel {
  id: number;
  uri: string
  userId: number;
  label: string;
  ingredients: string[];
}
