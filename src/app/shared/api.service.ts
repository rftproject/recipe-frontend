import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders } from "@angular/common/http";
import {Observable} from "rxjs";
import {Recipe} from "../recipes/model/recipe";
import {FeedbackViewModel} from "../feedback/feedback.component";
import {Cart} from '../cart/model/cart';
import {CartViewModel} from '../cart/cart.component';
import {FavoriteViewModel} from '../favorite/favorite.component';
import {Favorite} from '../favorite/model/favorite';
import {LoginData, LoginResponse, RegisterData} from '../login/model/login';
import {RequestOptions} from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private BASE_URL = "http://localhost:8080/api/";
  // private ALL_RECIPES_URL = `${this.BASE_URL}\\chicken\\0\\1`;
  private ALL_RECIPES_URL = "http://localhost:8080/api/recipes/chicken/0/10";
  private SEND_FEEDBACK_URL = "http://localhost:8080/api/feedback";
  // private SAVE_TEST_RECIPE = `${this.BASE_URL}\\recipes`;

  //Favorites
  private GET_FAVORITES_OF_USER_URL = "http://localhost:8080/api/favorites/";
  private DELETE_FROM_FAVORITES_URL = "http://localhost:8080/api/favorites/delete"
  private ADD_TO_FAVORITES_URL = "http://localhost:8080/api/favorites/add"

  //CART
  private GET_CART_BY_USER = "http://localhost:8080/api/cart/";
  private DELETE_CART= "http://localhost:8080/api/cart/delete"
  private ADD_TO_CART="http://localhost:8080/api/cart/add";

  private REGISTER_URL="http://localhost:8080/token/register";
  private LOGIN_URL="http://localhost:8080/token/login";

  constructor(private http: HttpClient) {

  }

  getAllRecipes(ingr: string, from: number, to: number/*, token: string*/) : Observable<Recipe[]>{
    // const headers = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + token
    //   // 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('mpManagerToken'))
    // });
    return this.http.get<Recipe[]>(this.BASE_URL + "recipes/" + ingr + "/" + from + "/" + to/*, {headers: headers}*/);
  }


  postFeedback(feedback: FeedbackViewModel): Observable<any> {
    return this.http.post(this.SEND_FEEDBACK_URL, feedback);
  }

  getFavoritesOfUser(username: number): Observable<Favorite[]> {
    return this.http.get<Favorite[]>(this.GET_FAVORITES_OF_USER_URL  + username);
  }

  postDeleteFavoriteById(favoriteId: number): Observable<any> {
    return this.http.post(this.DELETE_FROM_FAVORITES_URL, favoriteId);
  }

  postAddToFavorites(fav: FavoriteViewModel): Observable<any> {
    return this.http.post(this.ADD_TO_FAVORITES_URL, fav);
  }

  getCartByUser(id: number): Observable<Cart[]> {
    return this.http.get<Cart[]>(this.GET_CART_BY_USER + id);
  }

  postDeleteCartById(cartId: number): Observable<any> {
    return this.http.post(this.DELETE_CART, cartId);
  }

  postAddToCart(cart: CartViewModel): Observable<any>{
    return this.http.post(this.ADD_TO_CART, cart);
  }

  postRegisterUser(regData: RegisterData ): Observable<any> {
    return this.http.post(this.REGISTER_URL, regData);
  }

  postLoginUser(loginData: LoginData): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(this.LOGIN_URL, loginData);
  }

}
