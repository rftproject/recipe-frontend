import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {LoginData, LoginResponse, RegisterData} from './model/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  regData: RegisterData = {
    username:'',
    password:'',
    email:'',
    name:''
  };

  loginData: LoginData ={
    username:'',
    password:'',
  };

 public actualUser: LoginResponse={
    username:'',  //EZ A USER ID
    token:''
  }


  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  public registerUser() {



    this.apiService.postRegisterUser(this.regData).subscribe(
      res => {

        location.reload();
      },
      err => {
        alert("Failed to register");
      }
    );
  }

    public setCookie(name: string, val: string) {
    const date = new Date();
    const value = val;

    // Set it expire in 7 days
    date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000));

    // Set it
    document.cookie = name+"="+value+"; expires="+date.toUTCString()+"; path=/";
  }



  public loginUser(){
    this.apiService.postLoginUser(this.loginData).subscribe(
      res=>{
        this.actualUser= res;
        this.setCookie('token' , this.actualUser.token);
        this.setCookie('userId' , this.actualUser.username);
        alert('token:  ' + this.actualUser.token);
        location.reload();
      },
      err=>{
        alert("failed to login");
      }
    )
  }

}

