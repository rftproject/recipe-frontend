export interface RegisterData {
  username: string;
  password: string;
  email: string;
  name: string;
}

export interface LoginData {
  username: string;
  password: string;
}

export interface LoginResponse {
  username: string;
  token: string;
}
