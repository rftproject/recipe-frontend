import { Component } from '@angular/core';
import {Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'recipe-ng-app';
  showLoadingIndicator = true;

  public checkLoadStatus(): boolean{
    return this.showLoadingIndicator;
  }

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) =>{
      switch (true) {
        case event instanceof NavigationStart:{
          this.showLoadingIndicator=true;
          // alert("elso" + this.showLoadingIndicator);
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationError:
        case event instanceof NavigationCancel:{
          this.showLoadingIndicator= false;
          // alert("masodik" + this.showLoadingIndicator)
          break;
      }
        default:{
          break;
        }
  }

});
  }}
