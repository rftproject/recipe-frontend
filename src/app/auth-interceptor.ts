import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  public getCookie(name: string) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");

    if (parts.length == 2) {
      return parts.pop().split(";").shift();
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.getCookie("token");
    req = req.clone({headers: req.headers.set('Content-Type', 'application/json').set('Authorization', 'Bearer' + token)});



    if (token) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization", "Bearer " + token )
      });
      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}
