export interface Favorite {
  id: number;
  uri: string;
  userId: number;
  label: string;
  ingredients: string[];
  image: string;
}
