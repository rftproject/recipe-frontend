import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {Recipe} from '../recipes/model/recipe';
import {ActivatedRoute} from '@angular/router';
import {Favorite} from './model/favorite';
import {Event ,Router, NavigationStart, NavigationEnd, Route} from '@angular/router';


@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {
  recipes: Recipe[] = [];
  favorites: Favorite[] = [];

  public getCookie(name: string) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");

    if (parts.length == 2) {
      return parts.pop().split(";").shift();
    }
  }

  constructor(private apiService: ApiService,
              private route: ActivatedRoute
              ) {
    this.getFavoritesOfUser(Number(this.getCookie('userId')),);}

  ngOnInit() {
  }

  public getFavoritesOfUser(userId: number){
    this.apiService.getFavoritesOfUser(userId).subscribe(
      res => {
        this.favorites = res;
      },
      error => {
        alert("Unable to get favorites")
      }
    );
  }

  deleteFavorite(cartId: number): void{
    this.apiService.postDeleteFavoriteById(cartId).subscribe(
      res => {
        location.reload();
      },
      res=>{
        alert("An error has occurred while deleting favorite");
      }
    );
  }

  openFavorite(fav: Favorite){

  }



}

export  interface FavoriteViewModel {
  id: number;
  uri: string;
  userId: number;
  label: string;
  ingredients: string[];
  image: string;
}
