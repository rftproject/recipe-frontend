class ingredients {
  text: string;
  weight: number;
}

export interface Recipe {
  id:number
  uri:string;
  label:string;
  image:string;
  source:string;
  url:string;
  shareAs:string;
  yield:string;
  dietLabels:string[];
  healthLabels:string[];
  ingredientLines:string[];
  ingredients:ingredients[];
  calories:number;
  totalWeight:number;
  totalTime:number;
}
