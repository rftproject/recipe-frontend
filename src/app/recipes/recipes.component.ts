import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Recipe} from "./model/recipe";
import {ApiService} from "../shared/api.service";
import {CartViewModel} from '../cart/cart.component';
import {Favorite} from '../favorite/model/favorite';
import {FavoriteViewModel} from '../favorite/favorite.component';
import {LoginComponent} from '../login/login.component';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

   token: string = localStorage.getItem('token');
  recipes: Recipe[] = [];

  actualRecipe: Recipe[] = [];

  cartModel: CartViewModel = {
    id: null,
    uri: '',
    userId: Number(this.getCookie('userId')),
    label: '',
    ingredients: []
  };

  favoriteModel: FavoriteViewModel = {
    id: null,
    uri: '',
    userId: Number(this.getCookie('userId')),
    label: '',
    ingredients: [],
    image: ''
  };

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  public getCookie(name: string) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");

    if (parts.length == 2) {
      return parts.pop().split(";").shift();
    }
  }

  ingredientName="";
  public getAllRecipes(){
    this.apiService.getAllRecipes(this.ingredientName, 0, 5).subscribe(
      res=> {
        this.recipes = res;
      },
      error =>{
        alert("An error has occured")
      }
    );
  }

  public setActualRecipe(actualRecipeFromHtml: Recipe){
    this.actualRecipe = [];
    this.actualRecipe.push(actualRecipeFromHtml);
}

  addToCart(rec: Recipe): void {
    this.cartModel.label = rec.label;
    this.cartModel.uri = rec.uri;
    this.cartModel.ingredients = rec.ingredientLines;
    this.apiService.postAddToCart(this.cartModel).subscribe(
      res => {
      },
      res => {
        alert("An error has occured while adding to cart");
      }
    );
  }

  addToFavorite(fav: Recipe): void {
    this.favoriteModel.label = fav.label;
    this.favoriteModel.ingredients = fav.ingredientLines;
    this.favoriteModel.uri = fav.uri;
    this.favoriteModel.image = fav.image;

    this.apiService.postAddToFavorites(this.favoriteModel).subscribe(
      res => {
      },
      res => {
        alert("An error has occured while adding to cart");
      }
    );
  }

}
